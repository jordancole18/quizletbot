package me.jordancole.quizletwritebot;

/**
 * Jordan Cole
 * Nov 27, 2018
 * Period 2
 */

public class Term {

	private String term, definition;
	
	public Term(String term, String definition){
		this.term = term;
		this.definition = definition;
	}
	
	public String getTerm(){
		return term;
	}
	
	public String getDefinition(){
		return definition;
	}
	
}
