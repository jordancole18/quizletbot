package me.jordancole.quizletwritebot.api;

import java.util.Timer;
import java.util.TimerTask;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;

/**
 * Jordan Cole
 * Nov 29, 2018
 * Period 2
 */

public class QuizletManager {

	private WebDriver browser;
	private boolean loggedIn;
	
	public QuizletManager(){
		
	}
	
	private void startBrowser(){
		if(browser != null){
			System.out.println("Browser is already started..");
			return;
		}
		FirefoxOptions options = new FirefoxOptions();
		options.setCapability("marionette", false);
		browser = new FirefoxDriver(options);
	}
	
	public void loginToQuizlet(String mainUrl, String username, String password, String termUrl){
		
		if(loggedIn){
			System.out.println("User is already logged in!");
			return;
		}
		
		browser.navigate().to(mainUrl);

		WebElement btn = browser.findElement(By.cssSelector(".SiteHeader-signInBtn"));
		btn.click();

		WebElement usernameField = browser.findElement(By.name("username"));
		usernameField.click();
		usernameField.sendKeys(username);
		WebElement passwordField = browser.findElement(By.name("password"));
		passwordField.click();
		passwordField.sendKeys(password);
		passwordField.submit();
		Timer t = new Timer();
		t.schedule(new TimerTask() {
			public void run() {
				getTerms(browser, termUrl);
			}
		}, 2000);
		
	}
	
}
