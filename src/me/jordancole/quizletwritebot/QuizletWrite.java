package me.jordancole.quizletwritebot;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.Timer;
import java.util.TimerTask;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.support.ui.Select;

/**
 * Jordan Cole Nov 27, 2018 Period 2
 */

public class QuizletWrite {

	public static void main(String[] args) {
		System.setProperty("webdriver.gecko.driver", "/Library/Application Support/Adobe/SLCache/geckodriver");
		Scanner s = new Scanner(System.in);
		System.out.println("What is the url to quizlet?");
		String url = s.nextLine();
		System.out.println("What is your username?");
		String username = s.nextLine();
		System.out.println("What is your password?");
		String password = s.nextLine();
		String writeUrl = "";

		String[] data = url.split("/");
		System.out.println(data.length);

		if (data.length == 5) {
			for (int i = 0; i < data.length - 1; i++) {
				writeUrl += data[i] + "/";
			}
			writeUrl += "write";
		} else {
			if (url.endsWith("/")) {
				writeUrl += "write";
			} else {
				writeUrl += "/write";
			}
		}
		System.out.println(writeUrl);

		s.close();
		FirefoxOptions options = new FirefoxOptions();
		options.setCapability("marionette", false);
		WebDriver browser = new FirefoxDriver(options);
		loginToQuizlet(username, password, browser, url, writeUrl);
		s.close();
	}

	public static void loginToQuizlet(String username, String password, WebDriver browser, String mainUrl,
			String writeUrl) {
		browser.navigate().to(mainUrl);

		WebElement btn = browser.findElement(By.cssSelector(".SiteHeader-signInBtn"));
		btn.click();

		WebElement usernameField = browser.findElement(By.name("username"));
		usernameField.click();
		usernameField.sendKeys(username);
		WebElement passwordField = browser.findElement(By.name("password"));
		passwordField.click();
		passwordField.sendKeys(password);
		passwordField.submit();
		Timer t = new Timer();
		t.schedule(new TimerTask() {
			public void run() {
				getTerms(browser, writeUrl);
			}
		}, 2000);
	}

	public static void getTerms(WebDriver browser, String writeUrl) {
		System.out.println("Checking Terms...");
		try {
			String selector = ".UIDropdown";
			System.out.println("Setting to: Original");
			WebElement we = browser.findElement(By.cssSelector(selector));
			Select dropdown = new Select(we.findElement(By.className("UIDropdown-select")));
			dropdown.selectByIndex(0);
		} catch (Exception e) {

		}

		String count = browser.findElement(By.cssSelector(".SetPageHeader-termCount")).getAttribute("innerText");

		System.out.println(count);

		int countNum = Integer.parseInt(count.split(" ")[0]);

		List<Term> allTerms = new ArrayList<Term>();

		/*
		 * 
		 * https://quizlet.com/344052046/la-casa-21-flash-cards/
What is your username?
zhengallen21
What is your password?
az1456ft
		 * 
		 */
		
		for (int i = 1; i <= countNum; i++) {
			String termSel = "div.SetPage-term:nth-child(" + i
					+ ") > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > a:nth-child(1) > span:nth-child(1)";

			String defSel = "div.SetPage-term:nth-child(" + i
					+ ") > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(2) > div:nth-child(1) > a:nth-child(1) > span:nth-child(1)";
			try {
				WebElement setPageTerm = browser.findElement(By.cssSelector(termSel));
				WebElement setPageDef = browser.findElement(By.cssSelector(defSel));
				Term t = new Term(setPageTerm.getAttribute("innerText"), setPageDef.getAttribute("innerText"));
				System.out.println(i + ". " + t.getTerm() + " - " + t.getDefinition());
				allTerms.add(t);
			} catch (Exception e) {
				continue;
			}
		}
		
		System.out.println("Starting the write section...");
		doWriteSection(browser, writeUrl, allTerms);
	}

	public static void doWriteSection(WebDriver browser, String writeUrl, List<Term> allTerms) {
		browser.navigate().to(writeUrl);
		String progressBar = ".UIProgressBar--valid";
		WebElement bar = browser.findElement(By.cssSelector(progressBar));
		int currentProgress = Integer.parseInt(bar.getAttribute("aria-valuenow"));
		int maxProgress = Integer.parseInt(bar.getAttribute("aria-valuemax"));
		
		while (currentProgress < maxProgress) {
			try{
				String currentWord = browser.findElement(By.cssSelector(".qDef")).getAttribute("innerText");
				String answer = null;
				
				for(Term t : allTerms){
					if(t.getDefinition().equalsIgnoreCase(currentWord)){
						answer = t.getTerm();
						break;
					}
					if(t.getTerm().equalsIgnoreCase(currentWord)){
						answer = t.getDefinition();
						break;
					}
				}
				
				WebElement textArea = browser.findElement(By.cssSelector("#user-answer"));
				textArea.sendKeys(answer);

				WebElement answerBtn = browser.findElement(By.cssSelector("#js-learnModeAnswerButton"));
				answerBtn.click();
				
				try {
					Thread.sleep(100);
					System.out.println("Going to next question..");
					browser.findElement(By.cssSelector(".LearnModeMain-anyKey")).click();
					Thread.sleep(300);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}catch(Exception e){
				
			}

		}

	}

}
